from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = "login"
urlpatterns = [
    path('', views.indexView, name="login"),
    path('login/', LoginView.as_view(), name = 'login_url'),
    path('register/',views.registerView, name='register_url'),
    path('dashboard/', views.dashboardView, name="dashboard_url"),
    path('logout/',LogoutView.as_view(next_page="login:login"), name="logout_url")
 ]   