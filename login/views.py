from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

def indexView(request):
    return render(request, 'registration/index.html')

@login_required
def dashboardView(request):
    return render(request, 'dashboard.html')

def registerView(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/user/login/')
    else:
        form = UserCreationForm() 
    return render(request, 'registration/register.html', {'form': form})
 