$(document).ready(function(){
    $('.save').hover(
        function(){
            $(this).css({ "background-color" : "rgba(206, 197, 174, 1)" });
        }, function(){
            $(this).css({ "background-color" : "rgba(239, 233, 219, 1)" });
    });
    $('#tombolcari').hover(
        function(){
            $(this).css({ "background-color" : "rgba(134, 121, 104, 1)" });
        }, function(){
            $(this).css({ "background-color" : "rgba(168, 153, 133, 1)" });
    });

    $('.post-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        let x = window.location.pathname.split('/')[2]
        $.post({
            url: `/tambah-menu/${x}/`,
            data: form.serialize(),
            beforeSend: function(){
                $('.loader').show();
            },
            success: function(response) {
                    $('.loader').hide();
                },
            error: function(response) {
                    // form.html(data);
                    alert("ERROR");
                }
        });
    });

    $('#search').keyup(function(){
        let x = window.location.pathname.split('/')[2]
        $.get({
            url: `/menu/${x}/`,
            data: {"data" : $(this).val().toLowerCase()},
            dataType: 'json',
            success: function(response){
                $(".semua-menu").html(response.map( menu =>
                    `<div class = "col-sm d-flex align-items-center" style="flex-basis: 25%;">
                <div class="card" style="width: 15rem;">
                    <img
                    src= "${menu.foto}"
                    class="card-img-top"
                    alt="${menu.nama}"
                    />
                    <div class="card-body">
                        <p class="card-text">
                        <p> ${menu.nama} </p>
                        <p> ${menu.harga} </p>
                    </div>
                </div>
            </div>`
                ))
            }
        })
    });
    $(".kirimTombolMenu").on('click', function(event){
        event.preventDefault()
        let x = window.location.pathname.split('/')[2]
        $.post({
            url: `/tambah-menu-ajax/${x}/`,
            data: {
                'nama' : $("#id_Nama_Menu").val(),
                'harga' : $("#id_Harga_Menu").val(),
                'foto' : $("#id_Masukkan_Foto_Menu").val(),
                'csrfmiddlewaretoken': $("[name=csrfmiddlewaretoken]").val()
            },
            dataType: 'json',
            beforeSend: function(){
                $('.loader').show();
            },
            success: function(response) {
                alert(`Menu ${$("#id_Nama_Menu").val()} telah berhasil dimasukkan!`);
                $('.loader').hide();
                $("#id_Nama_Menu").val("");
                $("#id_Harga_Menu").val("")
                $("#id_Masukkan_Foto_Menu").val("")
            },
        });
    })
    

});


