"""cozzee URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from menupage import views
from menupage.views import TambahMenu, TampilanMenu

app_name = 'menupage'

urlpatterns = [
    path('tambah-menu/<int:id>/', views.TambahMenu, name="tambah_menu"),
    path('tambah-menu-ajax/<int:id>/', views.TambahMenuAjax, name="tambah_menu_ajax"),
    path('menu-page/<int:id>/', views.TampilanMenu, name='menu_page'),
    path('menu/<int:id>/', views.cari_menu, name='menu'),
    # path('hapusmenu-page/', views.HapusMenu)
]