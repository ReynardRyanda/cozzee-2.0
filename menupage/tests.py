from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpResponse
from .views import TambahMenu, TampilanMenu
from .forms import MenuForm
from .models import buatMenu
from landingpage.models import TambahToko

# Create your tests here.
class menuTest(TestCase):
    def test_menu_page_is_exist(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })
        response = Client().get('/menu-page/1/')
        self.assertEqual(response.status_code,200)

    def test_menu_page_not_exist(self):
        response = Client().get('/buatMenu/')
        self.assertEqual(response.status_code, 404)

    def test_menu_form_is_exist(self):
        response = Client().get('/tambah-menu/1/')
        self.assertEqual(response.status_code,200)

    def test_menu_form_not_exist(self):
        response = Client().get('/form-menu/')
        self.assertEqual(response.status_code, 404)

    def test_menu_page_func(self):
        found = resolve('/menu-page/1/')
        self.assertEqual(found.func, TampilanMenu)
    
    def test_menu_form_func(self):
        found = resolve('/tambah-menu/1/')
        self.assertEqual(found.func, TambahMenu)

    def test_menu_form_post(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })

        response = self.client.post('/tambah-menu/1/', {
            'Nama_Menu':"Matcha Latte",
            'Harga_Menu': "13.000",
            'Masukkan_Foto_Menu' : 'https://assets-pergikuliner.com/uploads/bootsy/image/14027/medium_medium_Caramel_Macchiato__www.allfoodsrecipes.com_.jpg',
            })
        hitungJumlah = buatMenu.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_menu_form_not_saved(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })

        response = self.client.post('/tambah-menu/1/', {
            'Nama_Menu':"Matcha Latte",
            'Harga_Menu': "13.000",
            'Masukkan_Foto_Menu' : '',
            })
        hitungJumlah = buatMenu.objects.all().count()
        self.assertEqual(hitungJumlah,0)
