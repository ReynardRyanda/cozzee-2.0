from django import forms 
from .models import buatMenu 

class MenuForm(forms.Form):
    Nama_Menu = forms.CharField(max_length = 30, label = "Nama Menu")
    Harga_Menu = forms.CharField(max_length = 20, label = "Harga Menu")
    Masukkan_Foto_Menu = forms.URLField(label = "Masukkan URL Foto Menu")