from django.test import TestCase, Client
from django.urls import resolve
from .views import feedback
from .forms import feedbackForm
from .models import Feedback


# Create your tests here.
class feedbackTest(TestCase):
    def test_feedback_is_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)
    
    def test_feedback_not_exist(self):
        response = Client().get('/Feedback/')
        self.assertEqual(response.status_code, 404)
    
    def test_using_feedbackForm(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedback)
    
    def test_uses_form_template(self):
        response = self.client.get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')
    
    def test_feedback_form(self):
        Feedback.objects.create(nama = "Reynard",
        usia = 19,
        kritik_dan_saran = "keren")

        hitungJumlah = Feedback.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_inputhasilForm(self):
        response = self.client.post('/feedback/', {
            'nama': "Reynard",
            'usia': "19",
            'kritik_dan_saran': "keren",
        })
        
        hitungJumlah = Feedback.objects.all().count()
        self.assertEqual(hitungJumlah,1)