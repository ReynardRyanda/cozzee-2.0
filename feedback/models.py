from django.db import models

# Create your models here.
class Feedback(models.Model):
    nama = models.CharField(max_length=50)
    usia = models.IntegerField()
    kritik_dan_saran = models.TextField(max_length=500)