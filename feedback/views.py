from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import feedbackForm
from django.shortcuts import redirect
from .models import Feedback
from . import forms

# Create your views here.
def feedback(request):
    if request.method == 'POST':
        form = feedbackForm(request.POST)
        if form.is_valid():
            form.save()
            nama = form.cleaned_data['nama']
            usia = form.cleaned_data['usia']
            kritik_dan_saran = form.cleaned_data['kritik_dan_saran']
            return redirect('/feedback')
    else:
        form = feedbackForm()
    return render(request, 'feedback.html', {'form':form})

def kritik_saran(request):
    feedback = Feedback(
        nama = request.POST['nama'],
        usia = request.POST['usia'],
        kritik_dan_saran = request.POST['feedback']
    )

    feedback.save()
    return JsonResponse({"Status": "OK"})

def saran_kritik(request):
    feedbacks = Feedback.objects.filter(kritik_dan_saran__startswith=request.GET['nama'])
    if not feedbacks:
        return JsonResponse({'saran' : ''})
    response = {'saran' : feedbacks[0].kritik_dan_saran}
    return JsonResponse(response, safe=False)