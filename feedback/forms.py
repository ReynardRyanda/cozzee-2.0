from django import forms
from .models import Feedback

class feedbackForm(forms.ModelForm):
    nama = forms.CharField(label = 'Nama', required=True)
    usia = forms.IntegerField(label = 'Usia', required=True)
    kritik_dan_saran = forms.CharField(label = 'Kritik dan Saran', widget=forms.Textarea, required=True)
    class Meta:
        model = Feedback
        fields = ('nama', 'usia', 'kritik_dan_saran')
        widgets = {'kritik_dan_saran': forms.Textarea()}
