from django.db import models

class TambahToko(models.Model):
    nama = models.CharField(max_length = 30)
    alamat = models.CharField(max_length = 100)
    jam_operasional = models.CharField(max_length = 50)
    nomor_telepon = models.CharField(max_length = 15)
    harga_per_orang = models.CharField(max_length = 20)
    fasilitas = models.TextField(max_length = 200)
    foto_url = models.URLField()

    def __str__(self):
        return self.nama
