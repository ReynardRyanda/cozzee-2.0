$(document).ready(function () {
    $('.save').hover(
        function () {
            $(this).css({ "background-color": "rgba(206, 197, 174, 1)" });
        }, function () {
            $(this).css({ "background-color": "rgba(239, 233, 219, 1)" });
        });
    $('#tombolcari').hover(
        function () {
            $(this).css({ "background-color": "rgba(134, 121, 104, 1)" });
        }, function () {
            $(this).css({ "background-color": "rgba(168, 153, 133, 1)" });
        });
    $('#search').keyup(function () {
        $.get({
            url: `/cariKedai/`,
            data: { "data": $(this).val().toLowerCase() },
            dataType: 'json',
            success: function (response) {
                $(".tabelToko").html(response.map(toko => 
                    `<div class="col-sm d-flex justify-content-center py-2">
    <div class="card" style="width: 18rem;">
      <a href="/toko/${toko.id}/"
        ><img
          src="${toko.foto}"
          class="card-img-top"
          alt="${toko.nama}"
          style="height: 200px;"
      /></a>
      <div class="card-body">
        <p class="card-text">
          ${toko.nama}
        </p>
      </div>
    </div>
  </div>`
                ))
            }
        })
    });
    $(".buatToko").on("click", function(event){
        event.preventDefault();
        console.log("test2");
        var form = $(this);
        // console.log(form);
        let nama = $('#id_nama').val();
        let alamat = $('#id_alamat').val();
        let jam_operasional = $('#id_jam_operasional').val();
        let nomor_telepon = $('#id_nomor_telepon').val();
        let harga_per_orang = $('#id_harga_per_orang').val();
        let fasilitas = $('#id_fasilitas').val();
        let foto_url = $('#id_foto_url').val();
        $.post({
            url: `/ajaxInputKedai/`,
            data: {
                'nama' : nama,
                'alamat' : alamat,
                'jam_operasional' : jam_operasional,
                'nomor_telepon' : nomor_telepon,
                'harga_per_orang' : harga_per_orang,
                'fasilitas' : fasilitas,
                'foto_url' : foto_url,
            },
            // data: form.serialize(),
            headers: {'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value},
            // dataType:'json',
            success: function (data) {
                console.log(data);
                var result = "<div class='col-sm d-flex justify-content-center py-2'><div class='card' style='width: 18rem;'>"+
                "<a href='toko/"+data.id+"/'><imgsrc='"+data.foto_url+"' class='card-img-top'"+
                "alt='Telegram Coffee' style='height: 200px;'/></a><div class='card-body'><p class='card-text'>"+data.nama+
                "</p></div></div></div>"
                $('.tabelToko').append(result);
                alert("Toko "+data.nama+" sudah anda masukkan!")
            }
        });
    });

    
});