from django.shortcuts import render, redirect
from APPComment.models import simpanKomen
from APPComment.forms import formKomen
from django.http import JsonResponse
from .models import TambahToko
from .forms import TambahTokoForm

# Create your views here.

def index(request):
    context = {
        'kedai' : TambahToko.objects.all()
    }
    return render(request,'index.html', context)  

def toko(request, id, optional = None):
    kedai = TambahToko.objects.get(id = id)
    komen = simpanKomen.objects.filter(id_toko = id)

    if optional == None:
        countRasa = 0
        countSuasana = 0
        countPelayanan = 0
        for x in komen:
            countRasa += x.fieldRatingR
            countSuasana += x.fieldRatingS
            countPelayanan += x.fieldRatingP
        banyakIsi = simpanKomen.objects.filter(id_toko=id).count()
        if banyakIsi != 0:
            rataRataTotal = (countRasa+ countSuasana+ countPelayanan)/3
            countRasa = round(countRasa/banyakIsi, 1)
            countSuasana = round(countSuasana/banyakIsi, 1)
            countPelayanan = round(countPelayanan/banyakIsi, 1)
            rataRataTotal = round(rataRataTotal/banyakIsi, 1)
            formulir = formKomen()
            optional = {
                'generated_html' : formulir,
                'countR' : countRasa,
                'countS' : countSuasana,
                'countP' : countPelayanan,
                'rataTotal' : rataRataTotal
            }
        else:
            formulir = formKomen()
            optional = {
                'generated_html' : formulir,
                'countR' : 0.0,
                'countS' : 0.0,
                'countP' : 0.0,
                'rataTotal' : 0.0
            }

    return render(request,'toko.html', {'kedai' :kedai, 'komen' : komen, 'nilai': optional})


def tambahToko(request):
    form = TambahTokoForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('/')

    return render(request, 'tambahToko.html', {
    'form' : form
  })

def cariKedai(request):
    tokos = TambahToko.objects.filter(nama__startswith=request.GET['data'])
    response = []
    for toko in tokos:
        tmpvar = {'nama' : toko.nama, 'foto' : toko.foto_url, 'id' : toko.id}
        response.append(tmpvar)
    return JsonResponse(response, safe=False)

def ajaxInputKedai(request):
    form = TambahTokoForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            baru = form.save()

    a = request.POST['nama']
    b = request.POST['alamat']
    c = request.POST['jam_operasional']
    d = request.POST['nomor_telepon']
    e = request.POST['harga_per_orang']
    f = request.POST['fasilitas']
    g = request.POST['foto_url']
    allToko = TambahToko.objects.all()
    data = {
        'nama' : a,
        'alamat' : b,
        'jam_operasional' : c,
        'nomor_telepon' : d,
        'harga_per_orang' : e,
        'fasilitas' : f,
        'foto_url' : g,
        'id' : baru.id
    }

    return JsonResponse(data)
