from django.test import TestCase, Client
from django.urls import resolve
from .views import index,toko,tambahToko
from .forms import TambahTokoForm
from .models import TambahToko
from APPComment.models import simpanKomen

# Create your tests here.
class LandingPageUnitTest(TestCase):

    def test_index_html_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_html_url_exist2(self):
        response = Client().get('/tambahToko/')
        self.assertEqual(response.status_code, 200)

    def test_func(self):
        found = resolve('/tambahToko/')
        self.assertEqual(found.func, tambahToko)
    
    def test_template(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
            'id_toko' : 1
        })
        response = self.client.get('/toko/1')
        self.assertTemplateNotUsed(response, 'toko.html')
        

    def test_input(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })
        hitung = TambahToko.objects.all().count()
        self.assertEqual(hitung,1)
    
    def test_input2(self):
        response = self.client.post('/tambahToko/', {
            'nama':"",
            'alamat':'',
            'jam_operasional' : '',
            'nomor_telepon' : "",
            'harga_per_orang': '',
            'fasilitas':'',
            'foto_url' : ''
        })
        hitung = TambahToko.objects.all().count()
        self.assertEqual(hitung,0)

    def test_name_is_same_in_databse(self):
        toko = TambahToko.objects.create(        
            nama = "ayam",
            alamat = 'jl.wedana',
            jam_operasional = 'jam 7',
            nomor_telepon = "081311634553",
            harga_per_orang= 'Rp10.000',
            fasilitas='wifi, tenis meja',
            foto_url = 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        )

        self.assertEqual("ayam", str(toko))
    
    def test_per_toko_without_komen(self):
        toko = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })
        
        komen = simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 2)

        response = self.client.get('/toko/1/')
        self.assertEqual(200, response.status_code)

        self.assertContains(response, 'ayam')

    def test_per_toko_with_komen(self):
        toko = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })
        komen = simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 1)

        response = self.client.get('/toko/1/')
        self.assertEqual(200, response.status_code)

        self.assertContains(response, 'ayam')
        self.assertContains(response, 'rasanya manis, tempatnya keren')