from django.urls import path
from . import views

app_name = 'landingpage'

urlpatterns = [
  path('',views.index, name='index'),
  path('toko/<int:id>/',views.toko, name='toko'),
  path('tambahToko/',views.tambahToko, name='tambahToko'),
  path('cariKedai/',views.cariKedai, name='cariKedai'),
  path('ajaxInputKedai/',views.ajaxInputKedai, name='ajaxInputKedai'),
]