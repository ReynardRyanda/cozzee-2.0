$(document).ready(function(){
    $('#search').keyup(function(){
        var val = $(this).val().toLowerCase();
        $('.card').hide();
        $('.card').each(function(){
            var text = $(this).text().toLowerCase();
            if(text.indexOf(val) != -1){
                $(this).show();
            }
        })
    });

    $('.card-body').hover(
        function(){
            $(this).css({ "background-color" : "rgba(206, 197, 174, 1)" });
        }, function(){
            $(this).css({ "background-color" : "rgba(239, 233, 219, 1)" });
    });

});