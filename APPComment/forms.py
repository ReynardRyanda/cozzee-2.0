from django import forms

RATING_CHOICES = [
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
]

class formKomen(forms.Form):
    fieldJudulKomen = forms.CharField(label = 'Judul Komentar')
    fieldIsiKomen = forms.CharField(label = 'Komentar')
    fieldRatingR = forms.ChoiceField(
        choices=RATING_CHOICES,
        label='Rating Rasa',
        # widget=forms.Select(attrs={'class':'col-md-4'}),
    )
    fieldRatingS = forms.ChoiceField(
        choices=RATING_CHOICES,
        label='Rating Suasana',
        # widget=forms.Select(attrs={'class':'col-md-4'}),
    )
    fieldRatingP = forms.ChoiceField(
        choices=RATING_CHOICES,
        label='Rating Pelayanan',
        # widget=forms.Select(attrs={'class':'col-md-4'}),
    )