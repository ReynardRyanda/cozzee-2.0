from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from .forms import formKomen
from .models import simpanKomen
from landingpage.views import toko
import json

def index(request, id):
    formulir = formKomen()
    argument = {
        'generated_html' : formulir,
        'id' : id
    }
    return render(request, 'input.html', argument)

def inputKomen(request, id):
    a = request.POST.get('fieldJudulKomen')
    b = request.POST.get('fieldIsiKomen')
    c = request.POST.get('fieldRatingR')
    d = request.POST.get('fieldRatingS')
    e = request.POST.get('fieldRatingP')
    f = id

    Komen = simpanKomen(
        fieldJudulKomen = a,
        fieldIsiKomen = b,
        fieldRatingR = c,
        fieldRatingS = d,
        fieldRatingP = e,
        id_toko = f
    )
    Komen.save()
    countRasa = 0
    countSuasana = 0
    countPelayanan = 0
    komen = simpanKomen.objects.filter(id_toko = id)
    for x in komen:
        countRasa += x.fieldRatingR
        countSuasana += x.fieldRatingS
        countPelayanan += x.fieldRatingP
    banyakIsi = simpanKomen.objects.filter(id_toko=id).count()
    rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
    countRasa = round(countRasa/banyakIsi, 1)
    countSuasana = round(countSuasana/banyakIsi, 1)
    countPelayanan = round(countPelayanan/banyakIsi, 1)
    rataRataTotal = round(rataRataTotal/banyakIsi, 1)
    formulir = formKomen() 
    argumen = {
        'generated_html' : formulir,
        'countR' : countRasa,
        'countS' : countSuasana,
        'countP' : countPelayanan,
        'rataTotal' : rataRataTotal
    }
    return toko(request,id, argumen)

def ajaxInputKomen(request, id):
    a = request.POST['fieldJudulKomen']
    b = request.POST['fieldIsiKomen']
    c = request.POST['fieldRatingR']
    d = request.POST['fieldRatingS']
    e = request.POST['fieldRatingP']
    f = id
    Komen = simpanKomen(
        fieldJudulKomen = a,
        fieldIsiKomen = b,
        fieldRatingR = c,
        fieldRatingS = d,
        fieldRatingP = e,
        id_toko = f
    )
    Komen.save()
    data = {
        'fieldJudulKomen' : a,
        'fieldIsiKomen' : b,
        'fieldRatingR' : c,
        'fieldRatingS' : d,
        'fieldRatingP' : e,
        'id_toko' : f
    }

    return JsonResponse(data)

def ajaxCekNilai(request, id):
    komen = simpanKomen.objects.filter(id_toko = id)
    countRasa = 0
    countSuasana = 0
    countPelayanan = 0
    for x in komen:
        countRasa += x.fieldRatingR
        countSuasana += x.fieldRatingS
        countPelayanan += x.fieldRatingP
    banyakIsi = simpanKomen.objects.filter(id_toko=id).count()
    rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
    countRasa = round(countRasa/banyakIsi, 1)
    countSuasana = round(countSuasana/banyakIsi, 1)
    countPelayanan = round(countPelayanan/banyakIsi, 1)
    rataRataTotal = round(rataRataTotal/banyakIsi, 1)
    argumen = {
        'countR' : countRasa,
        'countS' : countSuasana,
        'countP' : countPelayanan,
        'rataTotal' : rataRataTotal
    }
    return JsonResponse(argumen)

def deleteKomen(request):
    all = simpanKomen.objects.all()
    for x in all:
        x.delete()
    countRasa = 0
    countSuasana = 0
    countPelayanan = 0
    komen = simpanKomen.objects.all()
    # for x in komen:
    #     countRasa += x.fieldRatingR
    #     countSuasana += x.fieldRatingS
    #     countPelayanan += x.fieldRatingP
    rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
    argumen = {
        'countR' : countRasa,
        'countS' : countSuasana,
        'countP' : countPelayanan,
        'rataTotal' : rataRataTotal,
        'komen' : komen
    }
    return render(request, 'hasil.html', argumen)

def rataRata(ratingR, ratingS, ratingP):
    return (ratingP+ratingR+ratingS)/3

# def tigaRata(request):
#     countRasa = 0
#     countSuasana = 0
#     countPelayanan = 0
#     komen = simpanKomen.objects.all()
#     for x in komen:
#         countRasa += x.fieldRatingR
#         countSuasana += x.fieldRatingS
#         countPelayanan += x.fieldRatingP
#     banyakIsi = simpanKomen.objects.all().count()
#     rataRataTotal = rataRata(countRasa, countSuasana, countPelayanan)
#     countRasa = round(countRasa/banyakIsi, 2)
#     countSuasana = round(countSuasana/banyakIsi, 2)
#     countPelayanan = round(countPelayanan/banyakIsi, 2)
#     rataRataTotal = round(rataRataTotal/banyakIsi, 2)
#     argumen = {
#         'countR' : countRasa,
#         'countS' : countSuasana,
#         'countP' : countPelayanan,
#         'rataTotal' : rataRataTotal,
#         'komen' : komen
#     }
#     return render(request, 'hasil.html', argumen)


    
