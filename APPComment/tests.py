from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import inputKomen, deleteKomen
from .forms import formKomen
from .models import simpanKomen
from landingpage.models import TambahToko


class AppCommentTest(TestCase):
    def test_templateGaAda(self):
        response = self.client.get('/gajelas/')
        self.assertEqual(response.status_code,404)

    def test_formCommentExist(self):
        response = self.client.get('/indexComment/1/')
        self.assertEqual(response.status_code,200)
    
    def test_comment_form(self):
        simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 1)

        hitungJumlah = simpanKomen.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_delete_comment(self):
        simpanKomen.objects.create(fieldJudulKomen = "Kopinya enak bro",
        fieldIsiKomen = "rasanya manis, tempatnya keren",
        fieldRatingR = 3,
        fieldRatingS = 4,
        fieldRatingP = 5,
        id_toko = 1)

        allComment = simpanKomen.objects.all()

        response = self.client.get('/delete/')
        hitungJumlah = simpanKomen.objects.all().count()
        self.assertEqual(response.status_code,200)
        self.assertEqual(hitungJumlah,0)
    
    def test_inputhasilForm(self):
        response = self.client.post('/tambahToko/', {
            'nama':"ayam",
            'alamat':'jl.wedana',
            'jam_operasional' : 'jam 7',
            'nomor_telepon' : "081311634553",
            'harga_per_orang': 'Rp10.000',
            'fasilitas':'wifi, tenis meja',
            'foto_url' : 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
        })
        response = self.client.post('/input/1/', {
            'fieldJudulKomen':"Kopinya enak bro",
            'fieldIsiKomen':'Rasanya manis, tempatnya nyaman',
            'fieldRatingR' : '3',
            'fieldRatingS' : "4",
            'fieldRatingP': '5',
            'id_toko':'1',
            })
        
        hitungJumlah = simpanKomen.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    # def test_ajaxInputKomen(self):
    #     toko = TambahToko(
    #         nama="ayam",
    #         alamat='jl.wedana',
    #         jam_operasional = 'jam 7',
    #         nomor_telepon = "081311634553",
    #         harga_per_orang= 'Rp10.000',
    #         fasilitas='wifi, tenis meja',
    #         foto_url = 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
    #     )
    #     toko.save()
    #     response = self.client.post('ajaxInput/1/', {
    #         'fieldJudulKomen':"Kopinya enak bro",
    #         'fieldIsiKomen':'Rasanya manis, tempatnya nyaman',
    #         'fieldRatingR' : '3',
    #         'fieldRatingS' : "4",
    #         'fieldRatingP': '5'
    #     })
    #     self.assertJSONEqual(str(response.content, encoding = 'utf8'),{
    #         'fieldJudulKomen':"Kopinya enak bro",
    #         'fieldIsiKomen':'Rasanya manis, tempatnya nyaman',
    #         'fieldRatingR' : '3',
    #         'fieldRatingS' : "4",
    #         'fieldRatingP': '5',
    #         'id_toko':'1'
    #     })
        
    #     # self.assertJSONEqual(response.content, "{'fieldJudulKomen': 'Kopinya enak bro'}")
    #     hitungJumlah = simpanKomen.objects.all().count()
    #     self.assertEqual(hitungJumlah,0)


    