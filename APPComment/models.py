from django.db import models

class simpanKomen(models.Model):
    fieldJudulKomen = models.CharField(max_length = 20, default = " ")
    fieldIsiKomen = models.CharField(max_length = 50, default = " ")
    fieldRatingR = models.IntegerField(default = 0)
    fieldRatingS = models.IntegerField(default = 0)
    fieldRatingP = models.IntegerField(default = 0)
    id_toko = models.IntegerField()
